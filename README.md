# nodeExam

Evaluation Node.js
Sujet : L'API Books API

# Install 
```
npm install 
```

# Run the app
```
node index
```

# REST API

``` 
get /api/books 
post /api/books 

get /api/books/:id
put /api/books/:id
delete /api/books/:id
```