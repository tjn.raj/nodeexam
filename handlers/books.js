const db = require('../models');

// Post one book
exports.createBook = async (req, res) => {
  try {
    let newBook = await db.Book.create(req.body);
    return res.status(200).json({
      message: 'Nouvelle livre est crées avec succès',
      newBook
    });
  } catch (err) {
    return res.status(400).json({
      message: 'Oups! On a pas pu créer ton livre',
      error: err
    });
  }
};

// Get all books
exports.getAllBooks = async (req, res) => {
  try {
    //let socks = await db.Book.find();
    let books = req.query
      ? await db.Book.find(req.query)
      : await db.Book.find();
    return res.status(200).json(books);
  } catch(err) {
    return res.status(400).json({
      message: 'Oops could not find the books',
      error: err
    });
  }
}

// Get one book
exports.getOneBook = async (req, res) => {
  try {
    let thisbooks = await db.Book.findById(req.params.id);
    return res.status(200).json(thisbooks);
  } catch(err) {
    return res.status(400).json({
      message: 'Oops could not find this specific book',
      error: err.message
    });
  }
}

// Update one book
exports.updateOneBook = async (req, res) => {
  try {
    let bookToUpdate = await db.Book.findByIdAndUpdate(
      req.params.id,
      {
        $set: req.body
      },
      {
        new : true
      }
    );
    return res.status(200).json({
      message: 'Youpi livre bien mdifié',
      bookToUpdate
    })
  } catch(err) {
    return res.status(400).json({
      message: 'Oops could not modify this specific book',
      error: err.message
    });
  }
}

// Delete one book
exports.deleteOneBook = async (req, res) => {
  try {
    let bookToDelete = await db.Book.findByIdAndRemove(req.params.id);
    return res.status(200).json('Book deleted!');
  } catch(err){
    return res.status(400).json({
      message: 'Oops could not delete this specific book',
      error: err.message
    });
  }
}
