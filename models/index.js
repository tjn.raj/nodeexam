const mongoose = require('mongoose');

// Connecting to db
mongoose.connect('mongodb://localhost:27017/booksdb', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
.then(() => console.log('connected to books DB....'))
.catch(err => console.log(`Error connecting to db : ${err}`));

// Export all models
module.exports.Book = require('./book');