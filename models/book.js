const mongoose = require('mongoose');

// Schema of Book
const bookSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 300,
  },
  author: {
    type: String,
    required: true,
    minlength: 10,
    maxlength: 400,
  },
  categories: {
    type : [String],
    required: true,
    enum : ['sf', 'fantasy', 'polar', 'thriller', 'novel', 'marketing', 'business', 'non-fiction', 'fiction'],
    validate: {
      validator: function(v) {
        return v && v.length > 0;
      },
      message: 'Book should have at least one category'
    }
  },
  stock: {
    type: Number,
    required: true,
    min: 0,
  },
  publishDate: {
    type: Date,
    default: Date.now,
  },
  isBestSeller: {
    type: Boolean,
    default: false,
  },
});

// Model
const Book = mongoose.model('Book', bookSchema);

// Export
module.exports = Book;