const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const booksRoutes = require('./routes/books');

// Middleware(s)
app.use(express.json());

// First route :)
app.get('/', (req,res) => {
  res.send('Welcome to Books world!');
});

app.use('/api/books', booksRoutes);

app.listen(port, () => {
  console.log(`Listening to socks on port ${port}`);
});